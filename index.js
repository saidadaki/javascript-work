/**bai 1
 * input: nhap vao so ngay lam viec
 * math: nhan so ngay lam viec voi 100000
 * output: tong so luong
 */
const dailyWage = 100000;
var day = prompt("task1: How many work-day is that? ");
var salary = parseFloat(day)*dailyWage;
console.log("Tong luong la: ", salary);

/**bai 2
 * input: luu du lieu 5 so thuc tu user
 * math: cong 5 so thuc sau do chia cho 5 de lay so trung binh
 * output: gia tri trung binh
 */

var num1 = prompt("task2: Enter 1st number ");
var num2 = prompt("task2: Enter 2nd number ");
var num3 = prompt("task2: Enter 3rd number ");
var num4 = prompt("task2: Enter 4th number ");
var num5 = prompt("task2: Enter 5th number ");
var average = (parseFloat(num1) + parseFloat(num2) + parseFloat(num3) + parseFloat(num4) + parseFloat(num5))/5;
console.log("So trung binh la: ",average);

/**bai 3
 * input: nhap vao so luong USD
 * math: nhan so luong USD cho const gia USD=23500
 * output: so luong bang VND
 */
const USD = 23500;
var number = prompt("task3: how much USD do you have");
var vnd = parseFloat(number)*USD;
console.log("So tien quy doi ra vnd la: ", vnd);

/**bai 4 
 * input: nhap chieu dai va chieu rong
 * math: tinh chu vi = (chieu dai + chieu rong)*2, tinh dien tich = chieu dai*chieu rong
 * output: in ra chu vi va dien tich
*/
var dai = prompt("task4: Nhap vao chieu dai ");
var rong = prompt("task4: Nhap vao chieu rong");
var chuvi = (parseFloat(dai)+parseFloat(rong))*2;
var dientich = parseFloat(dai)*parseFloat(rong);
console.log("chu vi la: ", chuvi);
console.log("dien tich la: ", dientich);

/**bai 5
 * input: nhap vao mot so co 2 chu so
 * math: lay chu so do chia lay du cho 10 de ra so hang don vi, lay so input - so hang don vi sau do /10 de ra so hang chuc 
 * sau do tinh tong 2 so
 * output: tong 2 so cua input
 */
var inputnum = prompt("Nhap vao so co 2 chu so: ");
if (inputnum > 99 || inputnum < 10) {
    console.log("Khong phai so co 2 chu so");
} else {
var hangdonvi = parseInt(inputnum)%10;
var hangchuc = (parseInt(inputnum)-hangdonvi)/10;
var tong2so = hangdonvi + hangchuc;
console.log("Tong 2 so la: ",tong2so);
}
